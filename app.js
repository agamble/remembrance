
/**
 * Module dependencies.
 */

var express = require('express')
, routes    = require('./routes')
, user      = require('./routes/user')
, http      = require('http')
, path      = require('path');
var async   = require('async');
var crypto  = require('crypto');

var mongodb = require("mongodb"),
mongoserver = new mongodb.Server("localhost", 27017)

var db_connector = new mongodb.Db("main", mongoserver);
// Global Vars

var SALT = "95D0276FBCC7571A50149112FDBF319D5399E2E7D4D84388A323263CA2EF37E9"

//register function



//calls from database, 
//returns array if exists
//returns null if no query
function recallFromDatabase (collection, key, value, callback) {
  var result = [];
  db_connector.open(function(err, db) {
    if (err) { throw err; }
    //creating an object of value and key, so that query can be properly handled
    var query = {};
    query[key] = value;

    db.collection(collection, function(err, collection) {
      collection.find(query).toArray(function(err, result){
          if (result[0]) {
            db.close()
            console.dir("The Database returned: " + result[0]["username"]);
            callback(null, result);
          } else if (!result[0]) {
            console.log("Database was queried for " + value + " and nothing was found.");
            result = null;
            callback(null, result);
            db.close();
          }
      })
    })    
  });        
}


//this function takes an object as a parameter, 
//so that multiple entries can be inserted at once
function writeToDatabase (collection, insertObject, callback) {

  db_connector.open(function(err, db) {
    if (err) { throw err; }
    db.collection(collection, {strict: false}, function(err, collection) {
      collection.insert(insertObject, {safe: true}, function(err, records){
        var insertID = records[0]._id;
        callback(null, insertID)
        db.close()

      });
    });
  });



}


function hashpassword (password, usertoken, callback) {
  
  if (usertoken == null) {
      var result;
      var encpass = crypto.randomBytes(48, function(ex, buf) {
        var usertoken = buf.toString('hex');
        console.log("A new user token has been generated and is: " + usertoken);
        var hashedpassword = crypto.createHash('sha256').update(password + usertoken).digest('hex');
        console.log("A new hashed password has been generated: " + hashedpassword);
        hashedpasswordarray = [hashedpassword, usertoken];
        console.log(hashedpasswordarray);
        callback(null, hashedpasswordarray);
      });
  } else {

    var hashedpassword = crypto.createHash('sha256').update(password + usertoken).digest('hex');
    console.log("hash generated as " + hashedpassword)
    callback(null, hashedpassword);

  }


}

function register (username, password) {
  
  async.waterfall([


    function(callback) {
      recallFromDatabase("login", "username", username, function(err, results){
        if (err) {error(err);}
        if (results) {
          callback(new Error("User Exists; exiting", results))
        } else {
          callback(null, username, password)
        }
      });      
    },
    //create password hash
    function(username, password, callback) {

      var usertoken = null;
      hashpassword(password, usertoken, function(result){
        console.log("password hashed");
        callback(null, username, result)
      });
    }, 

    //write user to database
    function(username, result, callback) {
      var insertObject = { username: username, hashedpass: result[0], usertoken: result[1] };
      writeToDatabase("login", insertObject, function(err, insertID){
        if (err) {error(err);}
        console.log("Data was written for " + username + " as " + insertID);
      })
    }
  ], 
  //waterfall callback
  function (err, results){
    if (err) {console.log(err);}
  }
  );
}

//finish register function

//login

function login(username, password){


async.waterfall([
  
  function (callback) {
    recallFromDatabase("login", "username", username, function (err, query){
      var queriedusertoken = query[0]["usertoken"];
      callback(null, username, password, queriedusertoken);
    })
  },

  function (username, password, queriedusertoken, callback) {
    hashpassword(password, queriedusertoken, function (err, hashedpassword){
     callback(null, username, hashedpassword)
    });
  },


  function (username, hashedpassword, callback){
    recallFromDatabase("login", "username", username, function (err, result){
      if (hashedpassword == result[0]["hashedpass"]) {
        console.log("User " + username + " has successfully logged in");
      } else {
        console.log("There was a failed login attempt for " + username);
        callback(new Error("An incorrect login attempt was made"), result)
      }
    });
  }
],   
  
  function (err, results){
    if (err) {console.log(err);}
  })


}















function error (err) {
  console.log(err);
}

var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 8080);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

app.get('/', routes.index);
app.get('/users', user.list);
app.get('/register', routes.register);
app.get('/login', routes.login);


app.post('/register', function(req, res, db) {

  var username = req.body.user.name,
      password = req.body.user.password;
  
  register(username, password);
  res.send("success");
});

app.post('/login', function (req, res) {

  var username = req.body.user.name,
      password = req.body.user.password;

  login(username, password);

});


http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});

/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index', { title: 'Express' });
};


exports.register = function(req, res){
  res.render('register');
};

exports.login = function (req, res){
	res.render('login');
}
